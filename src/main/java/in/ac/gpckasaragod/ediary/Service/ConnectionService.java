/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.ediary.Service;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author student
 */
public interface ConnectionService {
    
    String jdbcUrl ="jdbc:mysql://localhost:3306/";
    String databaseNAME = "DaiHelp";
    String connectionString = jdbcUrl+databaseNAME;
    String username = "root";
    String password = "mysql";
    public Connection getConnection() throws SQLException{
        return DriverManager.getConnection(connectionString,username,password);
    }    
}
